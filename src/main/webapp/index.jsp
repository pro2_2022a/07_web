<%@ page import="cz.uhk.web.servlet.Game" %>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
    <title>Příklady</title>

</head>

<body>

<h1>Příklady Java Web</h1>
<h2>Servlety</h2>
<ul>
    <li><a href="Servlet1">Servlet1</a> základní sestavení odpovědi</li>
    <li><a href="Servlet2?promenna1=hodnota1">Servlet2</a> - co najdeme v requestu</li>
    <li><a href="session.html">Session1</a> - zpracování session proměnných</li>
    <li><a href="TimeServlet">TimeServlet</a> - aktuální čas jako text</li>
    <li><a href="TimeImgServlet">TimeImgServlet</a> - aktuální čas jako ciferník</li>
</ul>
<h2>JSP a JSTL</h2>
<ul>
    <li><a href="jspex1.jsp">jspex1.jsp</a> - scriptlety (míchání Javy a HTML, cyklus) </li>
    <li><a href="jspex2.jsp">jspex2.jsp</a> - zpracování formulářových proměnných</li>
    <li><a href="jstlex1.jsp">jstlex1.jsp</a> - ukázka c:forEach cyklu JSTL</li>
    <li><a href="jstlex2.jsp">jstlex2.jsp</a> - zpracování proměnných formu pomocí Expression Language</li>
    <li><a href="jstlex3.jsp">jstlex3.jsp</a> - lokalizace výstupu pomocí JSTL</li>
</ul>
</body>
</html>