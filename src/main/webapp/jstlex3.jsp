<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<fmt:requestEncoding value="UTF-8"/>

<fmt:setLocale value="EN" scope="application" />

<fmt:bundle basename="messages">
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"><html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><fmt:message key="page1.title" /></title>
</head>
<body>
	<h1>
		<fmt:message key="page1.title" />
	</h1>
	<form action="jstlex3.jsp" method="post" enctype="application/x-www-form-urlencoded">
		<fmt:message key="person.name" />: <input type="text" name="jmeno"><br>
		<fmt:message key="person.surname" />: <input type="text" name="prijmeni"><br>
		<input
			type="submit" value="<fmt:message key="person.send"/>">
	</form>
	<hr>
	${param.jmeno },${param.prijmeni }

</body>
</html>
</fmt:bundle>