package cz.uhk.web.servlet;
import java.util.Locale;
import javax.servlet.*;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;

@WebServlet("/Session1")
public class Session1 extends HttpServlet {
    
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, java.io.IOException {
        HttpSession ses = request.getSession(true);
        String value = null;
        if (!ses.isNew()) {
            value = (String)ses.getAttribute("hodnota");
        }
        ses.setAttribute("hodnota",request.getParameter("hodnota"));
        response.setContentType("text/html");
        response.setLocale(new Locale("cs","CZ"));
        java.io.PrintWriter out = response.getWriter();
        
        if (value!=null) {
            out.println("Puvodni hodnota: <b>"+value+"</b><hr>");
        }
        out.println("Nova hodnota: <b>"+request.getParameter("hodnota")+"</b><hr>");
        out.println("<a href=\"session.html\">Zpet na form</a>");
        out.close();
    }
    
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, java.io.IOException {
        doGet(request, response);
    }
    
}
