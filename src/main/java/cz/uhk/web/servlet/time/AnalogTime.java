package cz.uhk.web.servlet.time;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.util.Calendar;
import java.util.TimeZone;

public class AnalogTime {
	BufferedImage img;
	int width, height;
	
	public AnalogTime(int width, int height) {
		this.width = width;
		this.height = height;
		img = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
	}
	
	public BufferedImage getCurrentTime(TimeZone tz) {
		
		Graphics2D g= (Graphics2D) img.getGraphics();
		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		
		g.setColor(Color.WHITE);
		g.fillRect(0, 0, width, height);
		
		drawClockFace(g);
		
		Calendar cal = Calendar.getInstance(tz);
		int hh = cal.get(Calendar.HOUR);
		int mm = cal.get(Calendar.MINUTE);
		int ss = cal.get(Calendar.SECOND);
		
		drawHands(g,hh,mm,ss);
		
		return img;
	}

	/**
	 * Maluje rucicky hodin
	 * @param g
	 * @param hh
	 * @param mm
	 * @param ss
	 */
	private void drawHands(Graphics2D g, int hh, int mm, int ss) {
		//stred kruznice
		int sx = width/2;
		int sy = height/2;
		//delky rucicek
		int lh = width/4;		// hodinovky
		int lm = (int) (0.375*width);	// minutovky
		//koncove body rucicek
		double uh = Math.PI*2/12; //uhlova odchylka mezi dvema celymi hodinami
		double um = Math.PI*2/60; //mezi 2 minutami
		//hodinovka
		double uhel = uh*(hh+mm/60)-Math.PI/2;
		int hx = (int) (lh*Math.cos(uhel));
		int hy = (int) (lh*Math.sin(uhel));
		g.setStroke(new BasicStroke(5, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND));
		g.setColor(Color.GRAY);
		g.drawLine(sx, sy, sx+hx, sy+hy);
		//minutovka
		uhel = um*mm-Math.PI/2;
		int mx = (int) (lm*Math.cos(uhel));
		int my = (int) (lm*Math.sin(uhel));
		g.drawLine(sx, sy, sx+mx, sy+my);
		//vterinovka
		uhel = um*ss-Math.PI/2;
		int vx = (int) (lm*Math.cos(uhel));
		int vy = (int) (lm*Math.sin(uhel));
		g.setStroke(new BasicStroke(1, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND));
		g.setColor(Color.RED);
		g.drawLine(sx, sy, sx+vx, sy+vy);
	}

	private void drawClockFace(Graphics2D g) {
		g.setStroke(new BasicStroke(3));
		g.setColor(Color.WHITE);
		g.fillOval(2, 2, width-4, height-4);
		g.setColor(Color.BLACK);
		g.drawOval(2, 2, width-4, height-4);
		g.setColor(Color.DARK_GRAY);
		//puntiky na celych hodinach
		int sx = width/2;
		int sy = height/2;double uh = Math.PI*2/12;
		for(double u=0; u<2*Math.PI; u+=uh) {
			int cx = (int) (Math.cos(u)*width*0.45);
			int cy = (int) (Math.sin(u)*width*0.45);
			g.fillOval(sx+cx-2, sy+cy-2, 4, 4);
		}
	}
	
}
