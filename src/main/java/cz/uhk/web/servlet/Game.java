package cz.uhk.web.servlet;

public class Game
{
    String[][] board = new String[20][20];
    boolean crossTurn = false   ;

    public String[][] getBoard() {
        return board;
    }

    public boolean isCrossTurn() {
        return crossTurn;
    }

    public void setCrossTurn(boolean crossTurn) {
        this.crossTurn = crossTurn;
    }
}
