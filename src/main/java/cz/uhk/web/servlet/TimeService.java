package cz.uhk.web.servlet;
import java.util.*;
import java.text.*;

/**
 * Trida poskytujici lokalni cas
 * 
 * @author Tomas Kozel
 *
 */
public class TimeService {
    Calendar cal = null;

    public TimeService() {
        cal = Calendar.getInstance();
    }
    
    public String getTimeString() {
        SimpleDateFormat form = new SimpleDateFormat("HH:mm:ss dd.MM.yyyy");
        return "Je prave "+form.format(cal.getTime());
    }
    
}
