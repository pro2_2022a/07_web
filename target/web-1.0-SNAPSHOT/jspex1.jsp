<%@ page import="cz.uhk.web.servlet.Game" %>
<%@page contentType="text/html"%>
<html>
<head><title>JSP - priklad 1</title>
<script>
    function send(x,y,val){
        var url = 'http://localhost:8080/web_war_exploded/Game?x='+x+'&y='+y+'&val='+val;
        fetch(url,{method:"POST"})
            .then(res=>{
                location.reload();
            });
    }
</script>
</head>
<body>
<%
    Game game = (Game) application.getAttribute("game");
    if(game == null)
    {
        game = new Game();
        application.setAttribute("game", game);
    }

    String[][] board = game.getBoard();
    out.println("<table border='1'>");
    for (int i=0; i<20; i++)
    {
        out.println("<tr>");
        for (int j = 0; j < 20; j++)
        {
            out.println("<td>");
            out.println("<button onclick='send("+i+","+j+",\"HEJ\")'>"+board[i][j]+"</button>");

            out.println("</td>");
        }
        out.println("</tr>");
    }
    out.println("</table>");

//    for(int i=1; i<=10; i++) {
//        out.println("<h1>nadpis cislo "+i+"</h1>");
//    }
%>
    
</body>
</html>
